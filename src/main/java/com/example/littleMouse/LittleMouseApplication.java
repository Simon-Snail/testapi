package com.example.littleMouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LittleMouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(LittleMouseApplication.class, args);
	}

}
